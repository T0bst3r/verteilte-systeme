package udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Scanner;

public class UDPClient {
    public static void main(String[] args) { // args give message contents and server hostname
        DatagramSocket aSocket = null;
        try {
            aSocket = new DatagramSocket();

            InetAddress aHost = InetAddress.getByName(args[0]);
            int serverPort = 6789;

            Scanner scanner = new Scanner(System.in);
            System.out.println("Client ready start sending Messages");
            String message = scanner.nextLine();

            while (message != null) {
                byte[] messageBytes = message.getBytes();
                DatagramPacket request = new DatagramPacket(messageBytes, message.length(), aHost, serverPort);
                aSocket.send(request);
                byte[] buffer = new byte[1000];
                DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
                aSocket.receive(reply);
                System.out.println("Reply: " + new String(reply.getData()));
                message = scanner.nextLine();
            }
        } catch (SocketException e) {
            System.out.println("Socket: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO: " + e.getMessage());
        } finally {
            if (aSocket != null) aSocket.close();
        }
    }
}