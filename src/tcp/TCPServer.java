package tcp;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

public class TCPServer {
    public static void main(String[] args) {
        ChatServer server = new ChatServer();
        server.run();
    }
}

class Message {
    String sender;
    String content;
    Date timestamp;

    public Message(String s, String c) {
        this.sender = s;
        this.content = c;
        this.timestamp = new Date();
    }

    @Override
    public String toString() {
        return "(" + timestamp + ") " + sender + ": " + content;
    }
}

@SuppressWarnings("deprecation")
class ChatServer extends Observable {

    int serverPort = 7896;
    ServerSocket listenSocket;

    public void run() {
        try {

            listenSocket = new ServerSocket(serverPort);
            while (true) {
                Socket clientSocket = listenSocket.accept();
                Connection c = new Connection(clientSocket, this);
                this.addObserver(c);
            }

        } catch (SocketException e) {
            System.out.println("Listen :" + e.getMessage());
            setChanged();
            notifyObservers(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addMessage(Message message) {
        System.out.println(message);
        setChanged();
        notifyObservers(message);
    }

    public void shutDown() {
        try {
            if (!listenSocket.isClosed()) {
                listenSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class Connection extends Thread implements Observer {
    BufferedReader in;
    BufferedWriter out;
    Socket clientSocket;
    ChatServer chat;
    String nickname;

    public Connection(Socket aClientSocket, ChatServer chat) {
        this.chat = chat;
        try {
            System.out.println("Connected to: " + aClientSocket.getInetAddress());
            clientSocket = aClientSocket;
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            this.start();
        } catch (IOException e) {
            System.out.println("Connection:" + e.getMessage());
        }
    }

    public void run() {

        String data = "";
        try {// an echo server

            out.write("Welcome to the Server. What's your name?\n");
            out.flush();
            nickname = in.readLine();
            out.write("Hey there " + nickname + "\n");
            out.flush();
            data = in.readLine();
            while (!data.isEmpty() && !data.equals(".stops.") && !data.equals(".stopc.")) {

                chat.addMessage(new Message(nickname, data));
                data = in.readLine();
            }
            if (data.equals(".stops.")) chat.shutDown();
            System.out.println("Exiting");
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        } finally {
            try {
                clientSocket.close();
            } catch (IOException e) {/*close failed*/}
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        Message message = (Message) arg;

//        if (message.sender.equals(nickname))return;

        try {
            if (message == null) {
                out.write(new Message("SERVER", "shutting down").toString() + "\n");
                out.flush();
                in.close();
                return;
            }
            out.write(message.toString() + "\n");
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}