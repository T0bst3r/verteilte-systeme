package tcp;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class TCPClient {
    public static void main(String[] args) {// arguments supply message and hostname of destination

        ChatClient client = new ChatClient(args[0]);
        client.start();
    }
}

class ChatClient extends Thread {

    String serverAddress;

    ChatClient(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public void run() {
        Socket s = null;
        try {
            int serverPort = 7896;
            s = new Socket(serverAddress, serverPort);
            DataInputStream in = new DataInputStream(s.getInputStream());
            DataOutputStream out = new DataOutputStream(s.getOutputStream());

            ChatSend sender = new ChatSend(out);
            ChatRead reader = new ChatRead(in);

            sender.start();
            reader.start();

            while (sender.isAlive() && reader.isAlive()) {
            }
            sender.shutdown();
            reader.shutdown();

        } catch (UnknownHostException e) {
            System.out.println("Sock:" + e.getMessage());
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        } finally {
            if (s != null) try {
                s.close();
            } catch (IOException e) {
                System.out.println("close:" + e.getMessage());
            }
        }
    }
}

class ChatSend extends Thread {

    DataOutputStream out;
    boolean running = true;

    ChatSend(DataOutputStream out) {
        this.out = out;
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Client ready start sending Messages");
        String message = scanner.nextLine();
        System.out.println("message = " + message);
        while (message != null && running) {
            try {
                out.write((message + "\n").getBytes());// UTF is a string encoding see Sn 4.3
                out.flush();
                message = scanner.nextLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void shutdown() {
        running = false;
    }

}

class ChatRead extends Thread {

    DataInputStream in;
    boolean running = true;

    ChatRead(DataInputStream in) {
        this.in = in;
    }

    public void run() {
        String data = null;
        try {
            do {
                data = in.readLine();
                System.out.println(data);
            } while (!data.contains("SERVER: shutting down") && running);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void shutdown() {
        running = false;
    }
}